class CreateStudyListItems < ActiveRecord::Migration
  def change
    create_table :study_list_items do |t|
      t.integer :link_id
      t.integer :user_id
      t.boolean :completed

      t.timestamps
    end
  end
end
