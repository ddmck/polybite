class CreateLinks < ActiveRecord::Migration
  def change
    create_table :links do |t|
      t.string :url
      t.string :user_id
      t.string :title
      t.string :author

      t.timestamps
    end
  end
end
