Feature: Adding a tag to a link
  In order to group links by topic
  As a User
  I want to add a tag to it

  Scenario: Adding a tag in the command link
    Given a link exists
    When I create a tag
    And tag a link
    Then the link should be tagged

  Scenario: Creating a tag on the link object
    Given a link exists
    When I create a tag on the link object
    Then the link should be tagged