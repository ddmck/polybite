Feature: Viewing user profile
  In order to view my details
  As an existing User
  I want to view my profile

  Scenario: Visting the user profile from the home page
    Given I am at the home page
    And I am an existing user
    And I am signed in
    When I click the my profile link
    Then I should see my profile