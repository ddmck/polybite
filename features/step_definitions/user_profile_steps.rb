When(/^I click the my profile link$/) do
  click_link "My profile"
end

Then(/^I should see my profile$/) do
  page.should have_content("Your profile")
end