Given(/^I am at the home page$/) do
  visit root_path
end

When(/^I click on the sign up link$/) do
  click_link "Sign Up"
end

When(/^fill in the sign up form$/) do
  fill_in "First name", :with => "Donald"
  fill_in "Last name", :with => "McKendrick"
  fill_in "Date of birth", :with => "09/10/1988"
  fill_in "Email", :with => "ddmckendrick@gmail.com"
  fill_in "Password", :with => "Example123"
  fill_in "Password confirmation", :with => "Example123"
  click_button "Sign up"
end

Then(/^I should be registered$/) do
  page.should have_content("Welcome! You have signed up successfully.")
end

Given(/^I am an existing user$/) do
  User.create(
    first_name: "Donald",
    last_name: "Mckendrick",
    email: "ddmckendrick@gmail.com",
    password: "Example123",
    password_confirmation: "Example123",
    date_of_birth: "10/10/1988"
  )
end

When(/^I click the sign in link$/) do
  click_link "Sign In"
end

When(/^fill in the sign in form$/) do
  fill_in "Email", :with => "ddmckendrick@gmail.com"
  fill_in "Password", :with => "Example123"
  click_button "Sign in"
end

Then(/^I should be signed in$/) do
  page.should have_content("Signed in successfully.")
end


Given(/^I am signed in$/) do
  visit new_user_session_path
  fill_in "Email", :with => "ddmckendrick@gmail.com"
  fill_in "Password", :with => "Example123"
  click_button "Sign in"
  page.should have_content("Signed in successfully.")
end

When(/^I click the sign out link$/) do
  click_link "Sign Out"
end

Then(/^I should be signed out$/) do
  page.should have_content("Signed out successfully.")
end