Given(/^at the new link page$/) do
  visit new_link_path
end

When(/^I fill in the new link form$/) do
  fill_in("Url", :with => "http://donrails.com/articles/1")
  click_button "Create Link"
end

Then(/^I should have added my link$/) do
  page.should have_content("Donald on Rails | Hobo's on Rails")
end

Given(/^at the links index page$/) do
  visit links_path
end

Given(/^there are links on the website$/) do
  visit new_link_path
  fill_in("Url", :with => "http://latentflip.com/twenty-seven/")
  click_button "Create Link"
end

When(/^I click on a link$/) do
  StudyListItem.create(user_id: 1, link_id: 1)
end

Then(/^it should be added to my study list$/) do
  StudyListItem.all.count.should be > 0
end