Given(/^a link exists$/) do
  @link = Link.create(url: "http://latentflip.com/twenty-seven/")
end

When(/^I create a tag$/) do
  @tag = Tag.create(name: "ruby")
end

When(/^tag a link$/) do
  Tagging.create(link_id: @link.id, tag_id: @tag.id)
end

When(/^I create a tag on the link object$/) do
  @link.tags.create(name: "ruby")
end

Then(/^the link should be tagged$/) do
  expect(@link.tags.first.name).to eq("ruby")
end