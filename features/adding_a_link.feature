Feature: Adding a link
  In order to share a link I like
  As a User
  I want to add it to the site

  Scenario: Adding a link from the new link page
    Given I am an existing user
    And I am signed in
    And at the new link page
    When I fill in the new link form
    Then I should have added my link

  Scenario: Adding a link from the link index page
    Given I am an existing user
    And I am signed in
    And at the links index page
    When I fill in the new link form
    Then I should have added my link