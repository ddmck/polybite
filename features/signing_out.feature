Feature: Signing out
  In order to end my session
  As a logged in User
  I want to sign out

  Scenario: Signing out from the home page
    Given I am an existing user
    And I am signed in
    And I am at the home page
    When I click the sign out link
    Then I should be signed out