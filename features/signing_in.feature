Feature: Signing in
  In order to gain acess to the site
  As an existing User
  I want to sign in

  Scenario: Signing in from the home page
    Given I am at the home page
    And I am an existing user
    When I click the sign in link
    And fill in the sign in form
    Then I should be signed in