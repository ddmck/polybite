Feature: Signing up
  In order to gain access to the site
  As a User
  I want to sign up

  Scenario: Signing up from the home page
    Given I am at the home page
    When I click on the sign up link
    And fill in the sign up form
    Then I should be registered