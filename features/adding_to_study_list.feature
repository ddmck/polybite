Feature: Adding a link to study list
  In order to track what I'm reading
  As an existing User
  I want to add a link that I click on to my study list

  Scenario: Adding a link to my study list from the links index
    Given I am an existing user
    And I am signed in
    And there are links on the website
    And at the links index page
    When I click on a link
    Then it should be added to my study list