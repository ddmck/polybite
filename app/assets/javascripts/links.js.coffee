# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$ ->
  $(".index-link").on "click", (ev) ->
    clicked = $(ev.target)
    link_id = clicked.data "link-id"
    user_id = clicked.data "user-id"
    $.post "/study_list_items", { user_id: user_id, link_id: link_id}