require 'nokogiri'
require 'open-uri'

class Link < ActiveRecord::Base
  belongs_to :user
  has_many :study_list_items
  has_many :taggings
  has_many :tags, through: :taggings

  def parse_url
    doc = Nokogiri::HTML(open(self.url))
    self.title = doc.title
    dom_url = Domainatrix.parse(self.url)
    root_url = url.split(".#{dom_url.public_suffix}")[0] + ".#{dom_url.public_suffix}"
    doc = Nokogiri::HTML(open(root_url))
    self.author = doc.title
  end

end
