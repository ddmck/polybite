class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  has_many :links
  has_many :study_list_items

  def full_name
    self.first_name + " " + self.last_name
  end

  def gravatar_img
    url = Gravatar.new(self.email).image_url
  end
end
