class LinksController < ApplicationController
  before_action :authenticate_user!, only: [:index, :show]

  def show
    @link = Link.find(params[:id])
  end

  def index
    @links = Link.all
  end

  def new
    @link = Link.new
  end

  def update
    @link = Link.find(params[:id])
    @link.update(link_params)
    
    redirect_to links_path 
  end

  def create
    @link = Link.new(link_params)
    @link.user_id = current_user
    @link.parse_url
    @link.save
    redirect_to links_path
  end

  private
  def link_params
    params.require(:link).permit(:url)
  end
end
