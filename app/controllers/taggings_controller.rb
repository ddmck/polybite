class TaggingsController < ApplicationController
  def create
    tag = Tag.find_by_name(params[:tagging][:tag])
    if tag
      Tagging.create(link_id: params[:tagging][:link_id], tag_id: tag.id)
    else
      tag = Tag.create(name: params[:tagging][:tag]) 
      Tagging.create(link_id: params[:tagging][:link_id], tag_id: tag.id)
    end
    redirect_to :back
  end
end
