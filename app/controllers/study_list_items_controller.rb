class StudyListItemsController < ApplicationController

  def create
    study_list_item = StudyListItem.create(user_id: params[:user_id], link_id: params[:link_id])
    render text: study_list_item.inspect
  end

end
